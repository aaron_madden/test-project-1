import React from 'react';
import { Layout } from 'antd';

import Header from './components/layout/Header';
import Widgets from './components/Widgets';
import TickerHolder from './components/layout/TickerHolder';

import './styles/index.css';

const { Content } = Layout;

const LayoutStyle = {
    height: '100%',
    width: '100%',
    backgroundColor: 'var(--AND-WHITE)',
    overflowX: 'hidden'
};

const ContentStyle = {
    height: '100%',
    margin: '2rem'
};

export default function App() {
    return (
        <Layout style={LayoutStyle}>
            <Header />
            <Content style={ContentStyle}>
                <Widgets />
            </Content>
            <TickerHolder />
        </Layout>
    );
}
