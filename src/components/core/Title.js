import React from 'react';
import PropTypes from 'prop-types';

export default function Title(props) {
    const textColor = props.secondary
        ? 'var(--text-secondary-color)'
        : 'var(--text-color)';

    const Heading = {
        color: textColor,
        margin: 0,
        fontWeight: 'bold',
        fontSize: props.size || '2.8rem'
    };

    return <h1 style={Heading}>{props.children}</h1>;
}

Title.propTypes = {
    children: PropTypes.any,
    secondary: PropTypes.bool,
    size: PropTypes.string
};
