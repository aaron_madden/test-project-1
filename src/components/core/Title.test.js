import React from 'react';
import { render, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Title from './Title';

afterEach(cleanup);

describe('Title Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with no children', () => {
            const { container } = render(<Title />);
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with children', () => {
            const { container } = render(<Title>Test</Title>);
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with children, custom size', () => {
            const { container } = render(<Title size='3.0rem'>Test</Title>);
            expect(container).toMatchSnapshot();
        });
    });

    describe('Unit Tests', () => {
        it('Injects font size', () => {
            const { getByRole } = render(<Title size='3.0' />);

            const title = getByRole('heading');
            expect(title).toHaveTextContent('');

            const styling = window.getComputedStyle(title);
            expect(styling.fontSize).toBe('3.0');
        });

        it('Text children are passed to heading element', () => {
            const { getByRole } = render(<Title>Child Test</Title>);
            const heading = getByRole('heading');
            expect(heading).toHaveTextContent('Child Test');
        });
    });
});
