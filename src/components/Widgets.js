import React from 'react';
import MembersWidget from './widgets/members/MembersWidget';
import SpotifyPlayingWidget from './widgets/spotify/SpotifyPlayingWidget';
import ProjectsWidget from './widgets/projects/ProjectsWidget';
import CampusCalendarWidget from './widgets/calendar/CampusCalendarWidget';
import MidpointCalendarWidget from './widgets/calendar/MidpointCalendarWidget';
import CredibleCalendarWidget from './widgets/calendar/CredibleCalendarWidget';

const ContainerStyle = {
    height: '100%',
    display: 'flex'
};

const MembersContainer = {
    ...ContainerStyle,
    width: '25%'
};

const DraggableContainer = {
    ...ContainerStyle,
    width: '75%',
    flexFlow: 'wrap'
};
export default class Widgets extends React.Component {
    constructor() {
        super();
        this.state = {
            order: ['campus', 'midpoint', 'spotify', 'credible', 'projects'],
            drag: {
                dragging: false,
                element: ''
            }
        };
    }

    getOrderOf = widget => {
        return this.state.order.indexOf(widget);
    };

    isDragging = widget => {
        const { dragging, element } = this.state.drag;
        return dragging && element === widget;
    };

    onDragStart = event => {
        const elementName = event.dataTransfer.getData('name');
        setTimeout(() => {
            this.setState({
                drag: {
                    dragging: true,
                    element: elementName
                }
            });
        }, 1);
    };

    onDragEnd = event => {
        this.setState({
            drag: {
                dragging: false,
                element: ''
            }
        });
    };
    
    reorder = (dragging, dropping) => {
        if(dragging !== dropping) {
            let newOrder = [...this.state.order];
            let oldPos = this.getOrderOf(dragging);
            let newPos = this.getOrderOf(dropping);

            newOrder.splice(oldPos, 1);
            newOrder.splice(newPos, 0, dragging);

            if(newOrder.indexOf('projects') === 2) return;

            this.setState({
                order: newOrder
            });
        }
    }

    onDragEnter = event => {
        const droppableElement = event.target.name;
        const { drag } = this.state;

        if (droppableElement !== drag.element) {
            this.reorder(drag.element, droppableElement);
        }
    };

    render() {
        const Widgets = [
            CampusCalendarWidget,
            MidpointCalendarWidget,
            SpotifyPlayingWidget,
            CredibleCalendarWidget,
            ProjectsWidget
        ];

        return (
            <div style={ContainerStyle}>
                <div style={MembersContainer}>
                    <MembersWidget />
                </div>
                <div style={DraggableContainer}>
                    {Widgets.map(Item => (
                        <Item
                            getOrder={this.getOrderOf.bind(this)}
                            onDragStart={this.onDragStart.bind(this)}
                            onDragEnter={this.onDragEnter.bind(this)}
                            onDragEnd={this.onDragEnd.bind(this)}
                            isDragging={this.isDragging.bind(this)}
                        />
                    ))}
                </div>
            </div>
        );
    }
}
