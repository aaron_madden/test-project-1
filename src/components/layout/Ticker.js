import React, { Component } from 'react';
import styled, { keyframes } from 'styled-components';

const ticker = {
    // height: '3rem',
    lineHeight: '4rem',
    backgroundColor: 'var(--primary-color)',
    color: 'var(--text-secondary-color)',
    fontFamily: 'BrownRegular',
    fontSize: '2rem',
    fontWeight: 'bold'
};

function scroll(props) {
    return keyframes`
    from {
        transform: translateX(${window.innerWidth + 24}px);
    }
    to {
        transform: translateX(-${props.tickerWidth + 24}px);
    }
    `;
}

const TickerText = styled.div`
    display: inline-block;
    position: relative;
    white-space: nowrap;
    animation: ${scroll} ${props => props.tickerDuration}s infinite linear;
`;

export default class Ticker extends Component {
    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
        let tickerWidth = document.getElementById('tickerText').offsetWidth;
        this.props.setTickerTextWidth(tickerWidth);
    }

    render() {
        return (
            <div style={ticker}>
                <TickerText
                    id='tickerText'
                    tickerWidth={this.props.tickerWidth}
                    tickerDuration={this.props.tickerDuration}
                >
                    {this.props.tickerText}
                </TickerText>
            </div>
        );
    }
}
