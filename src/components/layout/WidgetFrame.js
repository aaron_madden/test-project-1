import React from 'react';
import PropTypes from 'prop-types';
import Title from '../core/Title';

const FrameStyle = {
    height: '100%',
    borderRadius: 'inherit'
};

const HeaderStyle = color => {
    return {
        textAlign: 'center',
        backgroundColor: color,
        padding: 'auto',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '6rem',
        borderRadius: '4px 4px 0px 0px',
        boxShadow: '2px 2px 6px rgba(0, 0, 0, 0.4)'
    };
};

const BodyStyle = {
    height: 'calc(100% - 9rem)',
    margin: '1.5rem'
};

export default function WidgetFrame(props) {
    return (
        <div style={FrameStyle}>
            <div style={HeaderStyle(props.color)}>
                <Title secondary>{props.header}</Title>
            </div>
            <div style={BodyStyle}>{props.children}</div>
        </div>
    );
}

WidgetFrame.propTypes = {
    color: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};
