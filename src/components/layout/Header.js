import React from 'react';
import { Row, Col } from 'antd';
import Title from '../core/Title';

const header = {
    backgroundColor: 'var(--primary-color)',
    color: 'var(--text-secondary-color)',
    fontFamily: 'BrownRegular',
    padding: '1rem',
    boxShadow: '2px 2px 6px rgba(0, 0, 0, 0.4)'
};

const headerCampusName = {
    textAlign: 'left'
};

const headerTime = {
    textAlign: 'center'
};

const headerDate = {
    textAlign: 'right'
};

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentDateTime: new Date()
        };
    }

    componentDidMount = () => {
        setInterval(() => {
            this.setState({ currentDateTime: new Date() });
        }, 10000);
    };

    getCurrentTime = () => {
        const dateTime = this.state.currentDateTime;
        const options = {
            timeStyle: 'short'
        };

        return dateTime.toLocaleTimeString(undefined, options);
    };

    getCurrentDate = () => {
        const dateTime = this.state.currentDateTime;
        const options = {
            weekday: 'long',
            month: 'long',
            day: 'numeric'
        };

        return dateTime.toLocaleDateString(undefined, options);
    };

    render() {
        return (
            <div className='header' style={header}>
                <Row>
                    <Col span={8} style={headerCampusName}>
                        <Title secondary>Ada Campus</Title>
                    </Col>
                    <Col span={8} style={headerTime}>
                        <Title secondary>{this.getCurrentTime()}</Title>
                    </Col>
                    <Col span={8} style={headerDate}>
                        <Title secondary>{this.getCurrentDate()}</Title>
                    </Col>
                </Row>
            </div>
        );
    }
}
