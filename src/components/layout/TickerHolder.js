import React, { Component } from 'react';
import Ticker from './Ticker';
import { Button } from 'antd';

const API_URL = process.env.REACT_APP_API_URL;
export default class TickerHolder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tickerDuration: 8,
            tickerWidth: null,
            speed_control_shown: false
        };
    }

    getTickerText = () => {
        fetch(`${API_URL}/ticker/1`)
            .then(res => {
                return res.json();
            })
            .then(res => {
                console.log(res);
                this.setState({
                    tickerText: res.ticker_text,
                    is_active: res.is_active
                });
            });
    };

    componentDidMount() {
        this.getTickerText();
    }

    setTickerTextWidth = tickerTextWidth => {
        this.setState({
            tickerWidth: tickerTextWidth
        });
    };

    handleTickerClickSlow = () => {
        let new_duration = this.state.tickerDuration;
        new_duration++;
        this.setState({
            tickerDuration: new_duration
        });
    };

    handleTickerClickFast = () => {
        if (this.state.tickerDuration !== 1) {
            let new_duration = this.state.tickerDuration;
            new_duration--;
            this.setState({
                tickerDuration: new_duration
            });
        }
    };

    handleMouseEnter = () => {
        this.setState({
            speed_control_shown: true
        });
    };

    handleMouseLeave = () => {
        this.setState({
            speed_control_shown: false
        });
    };

    render() {
        if (this.state.is_active) {
            return (
                <div
                    onMouseEnter={this.handleMouseEnter}
                    onMouseLeave={this.handleMouseLeave}
                >
                    <Ticker
                        tickerDuration={this.state.tickerDuration}
                        tickerText={this.state.tickerText}
                        tickerWidth={this.state.tickerWidth}
                        setTickerTextWidth={this.setTickerTextWidth}
                    ></Ticker>
                    {this.state.speed_control_shown ? (
                        <div>
                            <Button
                                style={{
                                    position: 'fixed',
                                    bottom: '32px',
                                    left: '0'
                                }}
                                onClick={this.handleTickerClickSlow}
                            >
                                Slower
                            </Button>
                            <Button
                                style={{
                                    position: 'fixed',
                                    bottom: '0',
                                    left: '0'
                                }}
                                onClick={this.handleTickerClickFast}
                            >
                                Faster
                            </Button>
                        </div>
                    ) : null}
                </div>
            );
        } else {
            return null;
        }
    }
}
