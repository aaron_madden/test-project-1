import React from 'react';
import { render, cleanup } from '@testing-library/react';
import WidgetFrame from './WidgetFrame';

afterEach(cleanup);

describe('WidgetFrame Component', () => {
    describe('Snapshot Tests', () => {
        const color = 'red';
        const header = 'Test';
        const child = <div>Test Item</div>;

        it('Renders correctly with no children', () => {
            const { container } = render(
                <WidgetFrame color={color} header={header} />
            );
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with 1 child', () => {
            const { container } = render(
                <WidgetFrame color={color} header={header}>
                    {child}
                </WidgetFrame>
            );
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with many children', () => {
            const { container } = render(
                <WidgetFrame color={color} header={header}>
                    {child}
                    {child}
                    {child}
                </WidgetFrame>
            );
            expect(container).toMatchSnapshot();
        });
    });
});
