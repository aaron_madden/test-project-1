import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Widget from './Widget';

afterEach(cleanup);

describe('Widget Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with placeholder and no children', () => {
            const { container } = render(<Widget />);
            expect(container).toMatchSnapshot();
        });
    });
});
