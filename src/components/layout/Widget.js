import React from 'react';

const WrapperStyle = props => {
    return {
        padding: '2rem',
        height: props.height || '50%',
        width: props.width || '33.33%',
        order: props.getOrder ? props.getOrder(props.name) : 1,
        display: 'flex'
    };
};

const ContainerStyle = props => {
    const visible =
        props.isDragging && props.isDragging(props.name) ? 'hidden' : 'visible';

    return {
        width: '100%',
        height: '100%',
        display: 'block',
        boxShadow: '2px 2px 6px rgba(0, 0, 0, 0.2)',
        borderRadius: '4px',
        visibility: visible
    };
};

const PlaceholderStyle = {
    ...ContainerStyle,
    backgroundColor: 'white',
    color: 'rgba(0.25,0.25,0.25,0.6)',
    fontSize: '1.5rem',
    fontStyle: 'italic',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
};

const PlaceHolder = <div style={PlaceholderStyle}>Widget Placeholder</div>;

export default function Widget(props) {
    const onDragStart = event => {
        event.dataTransfer.setData('name', props.name);
        if (props.onDragStart) props.onDragStart(event);
    };

    const onDragEnter = event => {
        event.preventDefault();
        event.target.name = props.name;
        if (props.onDragEnter) props.onDragEnter(event);
    };

    const onDragEnd = event => {
        event.preventDefault();
        event.dataTransfer.setData('name', props.name);
        if (props.onDragEnd) props.onDragEnd(event);
    };

    return (
        <div style={WrapperStyle(props)}>
            <div
                onDragStart={onDragStart}
                onDragEnd={onDragEnd}
                onDragEnter={onDragEnter}
                draggable={!props.static}
                style={ContainerStyle(props)}
            >
                {props.children || PlaceHolder}
            </div>
        </div>
    );
}
