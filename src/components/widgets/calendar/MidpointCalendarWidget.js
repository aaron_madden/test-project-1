import React from 'react';
import CalendarWidget from './CalendarWidget';

import config from '../../../config';
const Midpoint = config.calendars.midpoint;

export default function MidpointCalendarWidget(props) {
    return (
        <CalendarWidget
            {...props}
            calendarId={Midpoint.calendarId}
            calendarName={Midpoint.name}
            name='midpoint'
        />
    );
}
