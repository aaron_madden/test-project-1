import React from 'react';
import { render, cleanup } from '@testing-library/react';
import CalendarEvent from './CalendarEvent';

afterEach(cleanup);

describe('CalendarEvent Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with default props', () => {
            const { container } = render(<CalendarEvent />);
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with an event', () => {
            const startDate = new Date('01/01/2020 12:00');
            const endDate = new Date('01/01/2020 15:00');
            const Event = {
                start: { dateTime: startDate.toISOString() },
                end: { dateTime: endDate.toISOString() },
                summary: 'Test Event',
                creator: { email: 'tester@testing.com' }
            };

            const { container } = render(<CalendarEvent event={Event} />);
            expect(container).toMatchSnapshot();
        });
    });
});
