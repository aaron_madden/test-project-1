import React from 'react';
// import PropTypes from 'prop-types';
import Title from '../../core/Title';

const EventContainer = {
    display: 'flex',
    padding: '1rem',
    alignItems: 'center',
    width: '100%'
};

const EventCard = {
    display: 'flex',
    boxShadow: '2px 2px 6px rgba(0, 0, 0, 0.2)',
    padding: '1rem',
    borderRadius: '4px',
    alignItems: 'center',
    width: '100%'
};

const TimeContainer = {
    width: 'fit-content',
    marginRight: '1rem'
};

const TimeStyle = {
    lineHeight: 'initial',
    fontSize: '1.5rem'
};

const TimeSeparator = {
    lineHeight: 'initial',
    fontSize: '2rem',
    textAlign: 'center'
};

const DetailsContainer = {
    paddingLeft: '1rem',
    borderLeft: '1px solid var(--AND-CHARCOAL)'
};

const defaultEvent = {
    start: { dateTime: '' },
    end: { dateTime: '' },
    summary: '',
    creator: { email: '' }
};

export default function CalendarEvent({ event = defaultEvent }) {
    const { start, end, summary, creator } = event;

    return (
        <div style={EventContainer}>
            <div style={EventCard}>
                <div style={TimeContainer}>
                    <h3 style={TimeStyle}>{start.dateTime.slice(11, -4)}</h3>
                    <h3 style={TimeSeparator}>-</h3>
                    <h3 style={TimeStyle}>{end.dateTime.slice(11, -4)}</h3>
                </div>
                <div style={DetailsContainer}>
                    <Title size='2.2rem'>{summary}</Title>
                    <h5>{creator.email}</h5>
                </div>
            </div>
        </div>
    );
}

// CalendarEvent.propTypes = {
//     event: PropTypes.exact({
//         start: PropTypes.exact({
//             dateTime: PropTypes.string.isRequired
//         }).isRequired,
//         end: PropTypes.exact({
//             dateTime: PropTypes.string.isRequired
//         }).isRequired,
//         summary: PropTypes.string.isRequired,
//         creator: PropTypes.exact({
//             email: PropTypes.string
//         })
//     })
// };
