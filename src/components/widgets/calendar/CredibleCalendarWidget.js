import React from 'react';
import CalendarWidget from './CalendarWidget';

import config from '../../../config';
const Credible = config.calendars.credible;

export default function CredibleCalendarWidget(props) {
    return (
        <CalendarWidget
            {...props}
            calendarId={Credible.calendarId}
            calendarName={Credible.name}
            name='credible'
        />
    );
}
