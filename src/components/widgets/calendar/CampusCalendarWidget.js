import React from 'react';
import CalendarWidget from './CalendarWidget';

import config from '../../../config';
const CampusCalendar = config.calendars.campus;

export default function CampusCalendarWidget(props) {
    return (
        <CalendarWidget
            {...props}
            calendarId={CampusCalendar.calendarId}
            calendarName={CampusCalendar.name}
            name='campus'
        />
    );
}
