import React, { Component } from 'react';
import { GoogleLogin } from 'react-google-login';
import Widget from '../../layout/Widget';
import WidgetFrame from '../../layout/WidgetFrame';
import CalendarEvent from './CalendarEvent';

var CLIENT_ID =
    '658638316219-hvc05tmgchc8rp93tbuhn9bp8hpdknc2.apps.googleusercontent.com';

var API_KEY = 'AIzaSyAeIbOxPlU8ON6uETGYV6Be1Krmkd9zOek';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = [
    'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'
];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = 'https://www.googleapis.com/auth/calendar.readonly';

const HiddenOverflow = {
    overflow: 'hidden',
    height: '100%'
};

const CenteredContent = {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
};

const LoginText = {
    fontSize: '17px',
    marginRight: '5px'
};

const NoEventsPlaceholder = (
    <div style={CenteredContent}>
        <h2>No more events scheduled today</h2>
    </div>
);

export default class CalendarWidget extends Component {
    static defaultProps = {
        calendarId:
            'and.digital_t8qlboec5m1rtkedu1t1u1k4u0@group.calendar.google.com',
        calendarName: 'Campus Calendar'
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false
        };
    }

    componentDidMount() {
        function initialise() {
            window.gapi.client.init({
                apiKey: API_KEY,
                clientId: CLIENT_ID,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES
            });
        }

        window.gapi.load('client', initialise);

        function awaitClientLoad(loadCal) {
            setTimeout(() => {
                loadCal();
            }, 2000);
        }
        awaitClientLoad(this.loadCalendarEventsIntoState);
    }

    onSuccess = response => {
        this.loadCalendarEventsIntoState();
    };

    onFailure = response => {
        console.log('Failed...');
        alert(response.error);
    };

    onLogoutSuccess = response => {
        console.log('Logged out');
        this.setState({
            isLoggedIn: false
        });
    };

    loadCalendarEventsIntoState = () => {
        this.fetchCalendarEvents().then(response => {
            this.setState({ events: response, isLoggedIn: true });
        });
    };

    fetchCalendarEvents = () => {
        let today = new Date();
        let eod = new Date();
        let calID = this.props.calendarId;
        eod.setHours(23, 59, 59, 999);
        return new Promise(function(resolve, reject) {
            window.gapi.client.calendar.events
                .list({
                    calendarId: calID,
                    timeMin: today.toISOString(),
                    timeMax: eod.toISOString(),
                    showDeleted: false,
                    singleEvents: true,
                    maxResults: 10,
                    orderBy: 'startTime'
                })
                .then(function(response) {
                    resolve(response.result.items);
                });
        });
    };

    render() {
        const { isLoggedIn, events } = this.state;
        const { calendarName } = this.props;

        let campusEvents;

        if (events && events.length > 0) {
            campusEvents = events.filter(event => {
                return event.start.dateTime;
            });

            campusEvents = campusEvents.map(event => {
                return <CalendarEvent key={event.id} event={event} />;
            });
        }

        return (
            <Widget {...this.props}>
                <WidgetFrame header={calendarName} color='var(--AND-GREEN)'>
                    <div style={HiddenOverflow}>
                        {!isLoggedIn ? (
                            <div style={CenteredContent}>
                                <GoogleLogin
                                    clientId={CLIENT_ID}
                                    className='max-line-h'
                                    onSuccess={this.onSuccess}
                                    onFailure={this.onFailure}
                                    cookiePolicy={'single_host_origin'}
                                >
                                    <h4 style={LoginText}>Log in to Google</h4>
                                </GoogleLogin>
                            </div>
                        ) : events.length > 0 && campusEvents.length > 0 ? (
                            campusEvents
                        ) : (
                            NoEventsPlaceholder
                        )}
                    </div>
                </WidgetFrame>
            </Widget>
        );
    }
}
