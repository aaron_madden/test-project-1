import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Project from './Project';

afterEach(cleanup);

describe('Project Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with default props', () => {
            const { container } = render(<Project />);
            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with a project', () => {
            const project = {
                name: 'Test Project',
                description: 'Test Project desc',
                progress: { status: 'In Testing', icon: 'code', color: 'red' },
                PO: { name: 'Tester', imageUrl: '' }
            };
 
            const { container } = render(<Project project={project} />);
            expect(container).toMatchSnapshot();
        });
    });
});
