import React from 'react';
import PropTypes from 'prop-types';
import { Col, Icon } from 'antd';
import Title from '../../core/Title';

const ProjectContainer = {
    padding: '1.5rem',
    height: '100%',
    width: '100%'
};

const ProjectStyle = {
    boxShadow: '2px 2px 6px rgba(0, 0, 0, 0.2)',
    padding: '1.5rem',
    borderRadius: '4px',
    display: 'flex',
    position: 'relative',
    height: '100%'
};

const DescStyle = {
    fontSize: '1.8rem'
};

const DividerContainer = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
};

const DividerStyle = {
    width: '1px',
    height: '86%',
    backgroundColor: 'var(--AND-CHARCOAL)'
};

const ImageContainer = {
    textAlign: 'center'
};

const POImageStyle = {
    width: '100px',
    height: '100px',
    borderRadius: '4px',
    margin: '5px',
    display: 'inline'
};

const ProgressStyle = color => {
    return {
        position: 'absolute',
        bottom: '0',
        left: '0',
        padding: '0.5rem 0.8rem',
        backgroundColor: color,
        fontWeight: 'bold',
        color: 'var(--text-color)',
        lineHeight: '22px',
        borderRadius: '0px 4px 0px 4px',
        fontSize: '1.8rem',
        width: '18rem'
    };
};

const IconStyle = {
    paddingRight: '5px'
};

const defaultProject = {
    name: '',
    description: '',
    progress: {
        status: '',
        icon: '',
        color: ''
    },
    PO: {
        name: '',
        imageUrl: ''
    }
};

export default function Project({ project = defaultProject }) {
    const { name, description, progress, PO } = project;

    return (
        <div style={ProjectContainer}>
            <div style={ProjectStyle}>
                <Col span={18}>
                    <Title>{name}</Title>
                    <h3 style={DescStyle}>{description}</h3>
                </Col>
                <Col span={1} style={DividerContainer}>
                    <div style={DividerStyle} />
                </Col>
                <Col span={5} style={ImageContainer}>
                    <h2>PO</h2>
                    <img style={POImageStyle} src={PO.imageUrl} alt={PO.name} />
                    <h1>{PO.name}</h1>
                </Col>
                <span style={ProgressStyle(progress.color)}>
                    <h4>
                        {progress.icon && (
                            <Icon style={IconStyle} type={progress.icon} />
                        )}
                        {progress.status}
                    </h4>
                </span>
            </div>
        </div>
    );
}

Project.propTypes = {
    project: PropTypes.exact({
        name: PropTypes.string,
        description: PropTypes.string,
        progress: PropTypes.exact({
            status: PropTypes.string,
            icon: PropTypes.string,
            color: PropTypes.string
        }),
        PO: PropTypes.exact({
            name: PropTypes.string,
            imageUrl: PropTypes.string
        })
    })
};
