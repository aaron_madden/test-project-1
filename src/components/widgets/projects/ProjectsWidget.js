import React from 'react';
import Widget from '../../layout/Widget';
import WidgetFrame from '../../layout/WidgetFrame';
import SlideShow from '../../animation/SlideShow';
import Project from './Project';

import config from '../../../config.json';
const Projects = config.campusProjects;

const ProjectsContainer = {
    height: '100%',
    overflow: 'hidden'
};

export default class ProjectsWidget extends React.Component {
    render() {
        return (
            <Widget name='projects' width='66.66%' {...this.props}>
                <WidgetFrame header='Campus Projects' color='var(--AND-PINK)'>
                    <div style={ProjectsContainer}>
                        <SlideShow>
                            {Projects.map(project => (
                                <Project key={project.name} project={project} />
                            ))}
                        </SlideShow>
                    </div>
                </WidgetFrame>
            </Widget>
        );
    }
}
