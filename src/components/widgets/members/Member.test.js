import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Member from './Member';

afterEach(cleanup);

describe('Member Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly', () => {
            const member = {
                first_name: 'A',
                last_name: 'Tester',
                role: 'PD',
                image_url: ''
            };
            const { container } = render(<Member member={member} />);

            expect(container).toMatchSnapshot();
        });
    });
});
