import React from 'react';
import { render, cleanup } from '@testing-library/react';
import MembersWidget from './MembersWidget';

afterEach(cleanup);

describe('MembersWidget Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with no members', () => {
            const { container } = render(<MembersWidget />);

            expect(container).toMatchSnapshot();
        });
    });
});
