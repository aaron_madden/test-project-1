import React from 'react';
import { Col, Avatar } from 'antd';

const MemberStyle = {
    display: 'flex',
    alignItems: 'center',
    padding: '0.75rem'
};

const NameStyle = {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    fontSize: '2rem'
};

const RoleStyle = {
    textAlign: 'right'
};

export default function Member(props) {
    const { first_name, last_name, role, image_url } = props.member;

    return (
        <div style={MemberStyle}>
            <Col span={4}>
                <Avatar
                    shape='square'
                    size='large'
                    icon='user'
                    src={image_url}
                />
            </Col>
            <Col span={16}>
                <h2 style={NameStyle}>
                    {first_name} {last_name}
                </h2>
            </Col>
            <Col span={4} style={RoleStyle}>
                <h4>{role}</h4>
            </Col>
        </div>
    );
}
