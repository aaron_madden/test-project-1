import React from 'react';
import Widget from '../../layout/Widget';
import WidgetFrame from '../../layout/WidgetFrame';
import Member from './Member';

const API_URL = process.env.REACT_APP_API_URL;

const MembersContainer = {
    overflow: 'hidden',
    height: '100%'
};

const ScrollRate = 30;
const ScrollDelay = 10000;

export default class MembersWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollComplete: false,
            prevScrollTop: 0,
            scrolling: false,
            scrollDown: true,
            crew: []
        };
        this.membersRef = React.createRef();
    }

    componentDidMount = () => {
        fetch(`${API_URL}/crew?current=true`, {
            method: 'get'
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    crew: json
                });
            });
        setInterval(this.scrollMembersList, ScrollDelay);
    };

    scrollMembersList = () => {
        if (this.state.scrolling) {
            return;
        }
        this.setState({
            scrolling: true,
            scrollingInterval: setInterval(this.executeScroll, ScrollRate)
        });
    };

    executeScroll = () => {
        const list = this.membersRef.current;
        const { scrollComplete, prevScrollTop, scrollDown } = this.state;

        if (scrollComplete) {
            clearInterval(this.state.scrollingInterval);
            this.setState({
                scrolling: false,
                scrollComplete: false,
                scrollDown: !scrollDown
            });
            return;
        }

        list.scrollTop = prevScrollTop;
        if (scrollDown) {
            this.setState({
                prevScrollTop: prevScrollTop + 1,
                scrollComplete:
                    list.scrollTop >= list.scrollHeight - list.offsetHeight
            });
        } else {
            this.setState({
                prevScrollTop: prevScrollTop - 1,
                scrollComplete: list.scrollTop === 0
            });
        }
    };

    render() {
        return (
            <Widget height='100%' width='100%' static>
                <WidgetFrame header='Campus Crew' color='var(--AND-BLUE)'>
                    <div style={MembersContainer} ref={this.membersRef}>
                        {this.state.crew.map(c => (
                            <Member key={c.google_id} member={c} />
                        ))}
                    </div>
                </WidgetFrame>
            </Widget>
        );
    }
}
