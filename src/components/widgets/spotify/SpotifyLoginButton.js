import React from 'react';

const LoginButtonStyle = {
    borderRadius: '50px',
    backgroundColor: '#1DB954',
    color: '#FFFFFF',
    fontSize: '16px',
    lineHeight: '22px',
    padding: '12px 32px',
    minWidth: '168px',
    display: 'inline-flex',
    border: 'none',
    margin: '5px'
};

const LogoStyle = {
    marginRight: '8px',
    width: '21px',
    height: '21px'
};

export default function SpotifyLoginButton(props) {
    return (
        <button onClick={props.onClick} style={LoginButtonStyle}>
            <img
                style={LogoStyle}
                src='images/spotify-icon-white.png'
                alt='spotify-login'
            />
            Log in to Spotify
        </button>
    );
}
