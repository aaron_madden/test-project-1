import React from 'react';
import { render, cleanup } from '@testing-library/react';
import SpotifyPlayingWidget from './SpotifyPlayingWidget';

afterEach(cleanup);

describe('SpotifyPlayingWidget Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly', () => {
            const { container } = render(<SpotifyPlayingWidget />);

            expect(container).toMatchSnapshot();
        });
    });
});
