import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../core/Title';

const HeadingStyle = {
    color: 'var(--AND-WHITE)'
};

const AlbumOverlay = {
    backgroundColor: 'rgba(0,0,0,0.6)',
    width: '85%',
    height: '85%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 'inherit'
};

const TextContainer = {
    padding: '3rem'
};

const defaultTrack = {
    name: '',
    artist: '',
    albumArtUrl: '',
    playing: false
};

export default function CurrentlyPlaying({
    track = defaultTrack,
    trackFetched = false
}) {
    const { name, artist, albumArtUrl, playing } = track;

    const AlbumStyle = {
        backgroundImage: `url(${albumArtUrl})`,
        width: '100%',
        height: '100%',
        backgroundSize: 'cover',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 'inherit'
    };

    const NoTrackPlaceholder = <Title secondary>Finding music...</Title>;

    return (
        <>
            {trackFetched && track !== defaultTrack ? (
                <div style={AlbumStyle}>
                    <div style={AlbumOverlay}>
                        <div style={TextContainer}>
                            <h4 style={HeadingStyle}>
                                {playing ? 'Now Playing:' : 'Last Played:'}
                            </h4>
                            <Title size={'1.8rem'} secondary>
                                {name}
                            </Title>
                            <Title size={'1.8rem'} secondary>
                                - {artist}
                            </Title>
                        </div>
                    </div>
                </div>
            ) : (
                NoTrackPlaceholder
            )}
        </>
    );
}

CurrentlyPlaying.propTypes = {
    track: PropTypes.exact({
        name: PropTypes.string,
        artist: PropTypes.string,
        albumArtUrl: PropTypes.string,
        playing: PropTypes.bool
    }),
    trackFetched: PropTypes.bool.isRequired
};
