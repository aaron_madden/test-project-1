import React from 'react';
import { render, cleanup } from '@testing-library/react';
import CurrentlyPlaying from './CurrentlyPlaying';

afterEach(cleanup);

describe('CurrentlyPlaying Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly with track & playing (no image)', () => {
            const track = {
                name: 'Test track name',
                albumArtUrl: '',
                artist: 'Test track artist',
                playing: true
            };
            const { container } = render(
                <CurrentlyPlaying track={track} trackFetched />
            );

            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with track & not playing (no image)', () => {
            const track = {
                name: 'Test track name',
                albumArtUrl: '',
                artist: 'Test track artist',
                playing: false
            };
            const { container } = render(
                <CurrentlyPlaying track={track} trackFetched />
            );

            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with no track fetched', () => {
            const { container } = render(
                <CurrentlyPlaying trackFetched={false} />
            );

            expect(container).toMatchSnapshot();
        });

        it('Renders correctly with no track but track fetched', () => {
            const { container } = render(<CurrentlyPlaying trackFetched />);

            expect(container).toMatchSnapshot();
        });
    });
});
