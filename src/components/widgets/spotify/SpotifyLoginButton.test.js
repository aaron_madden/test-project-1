import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import SpotifyLoginButton from './SpotifyLoginButton';

afterEach(cleanup);

describe('SpotifyLoginButton Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly', () => {
            const { container } = render(<SpotifyLoginButton />);
            expect(container).toMatchSnapshot();
        });
    });

    describe('Unit Tests', () => {
        it('Calls "onClick" prop on button click', () => {
            const onClick = jest.fn();
            const { getByText } = render(
                <SpotifyLoginButton onClick={onClick} />
            );

            const RenderedComponent = getByText('Log in to Spotify');
            fireEvent.click(RenderedComponent);
            expect(onClick).toHaveBeenCalledTimes(1);
        });
    });
});
