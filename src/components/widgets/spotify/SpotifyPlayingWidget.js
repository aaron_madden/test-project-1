import React from 'react';

import SpotifyWebApi from 'spotify-web-api-js';
import crypto from 'crypto';

import Widget from '../../layout/Widget';
import SpotifyLoginButton from './SpotifyLoginButton';
import CurrentlyPlaying from './CurrentlyPlaying';

const CLIENT_ID = '76818ef578894e5ba103ac11150b304f';
const SCOPE = 'user-read-playback-state';
const REACT_APP_SPOTIFY_REDIRECT_URI =
    process.env.REACT_APP_SPOTIFY_REDIRECT_URI;
const AUTH_ENDPOINT = 'https://accounts.spotify.com/authorize';

const SpotifyContainer = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#191414',
    color: '#FFFFFF',
    borderRadius: 'inherit'
};

export default class SpotifyPlayingWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accessToken: '',
            tokenType: '',
            authenticated: false,
            trackFetched: false,
            fetchingTrack: null,
            track: {},
            auth: {}
        };
    }

    componentDidMount() {
        this.initialiseAuth();
        setTimeout(this.onLoginClick, 2000);
    }

    initialiseAuth = () => {
        this.setState({
            auth: {
                authHash: '',
                authUrl: '',
                authWindow: null,
                cbInterval: null
            }
        });
    };

    onLoginClick = () => {
        this.handleLogin();

        var playingInterval = setInterval(this.fetchCurrentlyPlaying, 15000);
        this.setState({
            fetchingTrack: playingInterval
        });
    };

    handleLogin = () => {
        const HASH = crypto.randomBytes(16).toString('hex');
        var authUrl = new URL(AUTH_ENDPOINT);
        var authParams = {
            client_id: CLIENT_ID,
            response_type: 'token',
            redirect_uri: REACT_APP_SPOTIFY_REDIRECT_URI,
            state: HASH,
            scope: SCOPE
        };

        authUrl.search = new URLSearchParams(authParams).toString();
        var authWindow = window.open(
            authUrl,
            '_blank',
            'toolbar=0,menubar=0,width=500,height=800'
        );
        const cbInterval = setInterval(this.handleLoginCallback, 300);

        this.setState({
            auth: {
                authHash: HASH,
                authUrl: authUrl,
                authWindow: authWindow,
                cbInterval: cbInterval
            }
        });
    };

    handleLoginCallback = () => {
        const { authHash, authUrl, authWindow, cbInterval } = this.state.auth;

        if (!authWindow) return;

        try {
            const authHref = authWindow.location.href;
            if (authHref === authUrl || authHref === 'about:blank') {
                return;
            }

            clearInterval(cbInterval);
            const responseUrl = new URL(authHref);
            authWindow.close();
            this.initialiseAuth();

            if (!responseUrl.hash) {
                // authentication error - no hash fragment
                clearInterval(this.state.fetchingTrack);
                this.setState({ fetchingTrack: null });
                return;
            }

            const responseParams = new URLSearchParams(
                responseUrl.hash.substr(1)
            );
            if (responseParams.get('state') !== authHash) {
                // csrf attack - don't authenticate
                clearInterval(this.state.fetchingTrack);
                this.setState({ fetchingTrack: null });
                return;
            }

            this.setState({
                accessToken: responseParams.get('access_token'),
                authenticated: true,
                tokenType: responseParams.get('token_type')
            });

            this.fetchCurrentlyPlaying();
        } catch (error) {
            // catching cross-origin frame errors - ignore them
        }
    };

    fetchCurrentlyPlaying = () => {
        if (this.state.authenticated) {
            const spotifyApi = new SpotifyWebApi();
            spotifyApi.setAccessToken(this.state.accessToken);
            spotifyApi.getMyCurrentPlayingTrack({}, (err, data) => {
                if (err) {
                    // authentication - no longer valid, log back in
                    this.handleLogin();
                    return;
                }

                if (!data) {
                    // no current song playing - so don't set anything
                    return;
                }

                const { item, is_playing } = data;
                const { album, artists, name } = item;

                this.setState({
                    track: {
                        name: name,
                        artist: artists[0].name,
                        albumArtUrl: album.images[0].url,
                        playing: is_playing
                    },
                    trackFetched: true
                });
            });
        }
    };

    render() {
        const { authenticated, trackFetched, track } = this.state;

        return (
            <Widget name='spotify' {...this.props}>
                
                <div style={SpotifyContainer}>
                    {!authenticated && (
                        <h1>hello</h1>
                    )}

                    {authenticated && (
                        <CurrentlyPlaying
                            track={track}
                            trackFetched={trackFetched}
                        />
                    )}
                </div>
            </Widget>
        );
    }
}
