import React from 'react';
import { Transition } from 'react-spring/renderprops';

const DEFAULT_DELAY = 20000;

const Container = {
    position: 'relative',
    height: '100%',
    width: '100%'
};

const Slide = {
    width: '100%'
};

export default class SlideShow extends React.Component {
    constructor() {
        super();
        this.state = {
            slide: 0
        };
    }

    componentDidMount() {
        const delay = this.props.delay || DEFAULT_DELAY;
        setInterval(this.changeSlide, delay);
    }

    changeSlide = () => {
        let slide = this.state.slide;

        slide = (slide + 1) % this.props.children.length;
        this.setState({ slide: slide });
    };

    render() {
        return (
            <div style={Container}>
                <Transition
                    items={this.state.slide}
                    from={{ position: 'absolute', opacity: 0, width: '100%'}}
                    enter={{ opacity: 1 }}
                    leave={{ opacity: 0 }}
                >
                    {item => props => (
                        <div style={{ ...props, Slide }}>
                            {this.props.children[item]}
                        </div>
                    )}
                </Transition>
            </div>
        );
    }
}
