# Ada Campus Dashboard

Dashboard to be displayed on the midpoint TV in AH. Provides ANDi's with all
information they need about Campus, and what is going on.

## Getting Started

Clone the repo and install the node pacakages.

```
$ git clone git@gitlab.com:ANDigital/Ada/campus/campus-dashboard.git
$ cd campus-dashboard
$ npm i
```

Now get the project up and runnning with, and visit
[localhost](http://localhost:3000)

```
$ npm start
```

## Developer Notes

We are using the Prettier linter along with eslint to maintain high code
quality, so **please install these**, and alter the settings to read
configuration from a config file. Config located in `.prettierrc.js`. The eslint
config is located in `.eslintrc.js`, and the prettier config includes this file
itself.

We are also very loosely using the Ant-Design component library for development,
which means we are basically not using it. See the docs [here](https://ant.design/docs/react/introduce)
for information. We have definitely used the icons and perhaps the title but
not much else. We have wrote most of the base components ourselves, and can be
found throughout the repo inside `layout`, `core` and `animation` folders.

**React promotes reusability of components**, so please continue with this and
extract components into resuable ones where appropriate and possible.

## Testing

For tests, we are using a combination of **Jest** and **React Testing Library**
to test the React components. Check the
[Jest Docs](https://jestjs.io/docs/en/getting-started) and the
[React Testing Library Docs](https://testing-library.com/docs/react-testing-library/intro)
for more information about how to write tests in these.

To run the test suite simply run:

```
$ npm test
```

This then provides you with more options (displayed on screen) of running all
tests or running specific tests.

To regenerate outdated snapshots, press `'u'` inside the test suite, _after_ the
tests have run and either failed or produced outdated snapshots. Note: Snapshots
should only change if you have deliberately made a change to a component -
otherwise the snapshots shouldn't change, and so a failing test would infact
indicate an error.

For **test coverage whilst watching** files, run the test suite with the following
options, like so:

```
$ npm test -- --coverage
```

For the **overall test coverage**, run the test suite with the following options:

```
$ npm test -- --coverage --watchAll=false
```

or, using shorthand

```
$ npm run test-coverage
```

### Testing Structure

**Every React component should have a test file.** If your component is called
`MyComponent` then the test file should be placed in the same directory level
and be given the name `MyComponent.test.js`.

Each component **must have a snapshot test at the minimum**, and where
appropriate should have unit tests for testing the core functionality. Snapshots
are auto created by the testing suite and placed in a folder `__snapshots__`.
We want to commit this folder and its contents as the test suite will compare
the component with this snapshot and we need to keep the snapshot consistent.

We are using the **following testing structure** in our test files (the example
also contains an example snapshot test):

```js
import React from 'react';
import { render, cleanup } from '@testing-library/react';
// other imports
import MyComponent from './MyComponent';

afterEach(cleanup);

describe('MyComponent Component', () => {
    describe('Snapshot Tests', () => {
        it('Renders correctly', () => {
            const { container } = render(<MyComponent />);

            expect(container).toMatchSnapshot();
        });

        // more snapshot tests here with it blocks...
    });

    describe('Unit Tests', () => {
        it('Does some thing in some way', () => {
            // test code here and expects
            // expect().toMatch();
        });

        // more tests here using it('', () => {}) blocks...
    });

    // more describe blocks here...
});
```

Keep tests for 1 piece of functionality in separate `it` blocks, and group
together tests inside `describe` blocks. You can nest as many `describe`
blocks as you need.

Note: some components might have 1 or many snapshot tests, as well having 0 or
many unit tests.

### Missing Tests

You will notice there is no test file for the `ProjectsWidget` component -
There is a current problem with testing components which use react-spring. I
couldn't find a good fix for this, so avoiding testing this component for the
time being.

There is also no test file for `CalendarWidget` and the respective Calendars.
The Google API is loaded in via the index.html file in the project, which is
a temporary solution, and so it would need to be mocked in the tests. As this
is a temporary solution, there is no test file for now.

## Environment Variables

The dashboard pulls content from an API service, which accesses a database
currently containing who is on campus. The API endpoint URL needs to be
specified as an environment variable in a `.env` file as follows. Place the
file in the root directory.

```
REACT_APP_API_URL=add.endpoint.here
```

## Spotify API

The Spotify API authentication requires a callback URL which has to be
whitelisted on the the Spotify developers portal. Therefore to get the Spotify
widget to work, the react app currently needs to be run on port `3000`.
